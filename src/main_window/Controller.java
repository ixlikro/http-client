package main_window;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Request;
import view.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * The controller holds all UI-Elements, views and the model
 */
public class Controller {

    public static int INVALID = -1;
    public static int DEFAULT_PORT = 80;
    public static String WEB_VIEW_FILE_PATH = "ResponseBodyForWebView.html";

    //gui elements from the mainWindow.fxml file
    @FXML public Button inputSendRequest;
    @FXML public TextArea inputRequestArguments;
    @FXML public ToggleGroup inputRadioGroup;
    @FXML public RadioButton inputRadioButtonPOST;
    @FXML public RadioButton inputRadioButtonGet;
    @FXML public TextField inputPort;
    @FXML public TextField inputWebAddress;
    @FXML public ImageView inputWebAddressHelp;
    @FXML public ImageView inputRequestArgumentsHelp;
    @FXML public Button inputReset;
    @FXML public Label outputRequestHeader;
    @FXML public Label outputResponseHeader;
    @FXML public Label outputResponseBody;
    @FXML public Label outputLinkCount;
    @FXML public Button saveInFileBtn;
    @FXML public Button showInWebViewBtn;

    private List<View> allViews;
    private Request request;
    private Stage stage;

    public Controller() {
        allViews = new ArrayList<>();

        //add all views
        allViews.add(new InputView(this));
        allViews.add(new RequestHeaderView(this));
        allViews.add(new ResponseBodyView(this));
        allViews.add(new ResponseHeaderView(this));
        allViews.add(new ResponseBodyActionView(this));
    }


    public void setup(Stage stage) {
        this.stage = stage;
        request = new Request(this);
        performOnAllViews(View::setupUI);
    }


    public void sendRequest(){
        request.sendRequest();
    }

    public void updateUI(){
        performOnAllViews(View::updateUI);
    }

    public void reset(){
        request = new Request(this);
        performOnAllViews(View::resetUI);
    }

    public Request getRequest() {
        return request;
    }

    public Stage getStage() {
        return stage;
    }

    private void performOnAllViews(Consumer<View> consumer) {
        allViews.forEach(consumer);
    }

}
