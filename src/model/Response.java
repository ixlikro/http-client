package model;

import java.util.ArrayList;
import java.util.List;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Response {

    //credits for the regex goes to stema (https://stackoverflow.com/a/5713697/7823600)
    private static Pattern pattern = Pattern.compile("\\b((?:https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|])");

    private String header;
    private String body;

    public Response() {
        this.header = null;
        this.body = null;
    }

    public Response(String header, String body) {
        this.header = header;
        this.body = body;
    }

    /**
     * find all external links in the body string
     * @return a list with all external links from the body
     */
    public List<String> findLinksInBody(){
        List<String> ret = new ArrayList<>();

        if(body != null){
            Matcher matcher = pattern.matcher(body);

            while (matcher.find()){
                ret.add(body.substring(matcher.start(), matcher.end()));
            }
        }
        return ret;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
