package model;

public enum RequestState {
    NOT_SEND_YET,
    PENDING,
    TIMEOUT,
    ERROR,
    UNKNOWN_HOST,
    HEADER_ONLY,
    RESPONSE_RECEIVED
}
