package model;

import javafx.application.Platform;
import javafx.util.Pair;
import main_window.Controller;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class Request {

    private final RequestHeader reqHeader;
    private Response response;
    private RequestState state;
    private Controller controller;

    public Request(Controller controller) {
        this.controller = controller;
        reqHeader = new RequestHeader();
        response = null;
        state = RequestState.NOT_SEND_YET;
    }

    /**
     * sends the request and receive the response
     */
    public void sendRequest(){
        setState(RequestState.PENDING);
        CompletableFuture
                .supplyAsync(this::performRequest)
                .completeOnTimeout(new Pair<>(RequestState.TIMEOUT, new Response()) , 20, TimeUnit.SECONDS)
                .thenAccept(responsePair -> {
                    //execute callback on the main thread
                    Platform.runLater(() -> {
                        this.response = responsePair.getValue();
                        setState(responsePair.getKey());
                    });
                });
    }


    /**
     * set a new state and call the {@link Controller#updateUI()} method so all views will be up to date
     * @param newState the new state
     */
    private void setState(RequestState newState){
        state = newState;
        controller.updateUI();
    }

    /**
     * the worker method, this method will perform the actual request<br>
     * WARNING: Don't call this function from the main Thread, use {@link Request#sendRequest()} instead
     * @return a state and the response packed as pair
     */
    private Pair<RequestState, Response> performRequest(){

        RequestState retState = RequestState.RESPONSE_RECEIVED;
        Response retResponse = new Response();
        String reqHeaderString;
        String hostName;
        int port;

        //.buildHeader() and .getHostName() return a reference to a new String, so it's safe to synchronize just this 3 lines
        synchronized (reqHeader){
            reqHeaderString = reqHeader.buildHeader();
            hostName = reqHeader.getHostName();
            port = reqHeader.getPort();
        }
        if(reqHeaderString != null){

            Socket socket = null;
            PrintWriter writer = null;
            BufferedReader reader = null;
            StringBuilder stringBuilder = null;
            try {
                //send request
                socket = new Socket(hostName, port);
                writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
                writer.print(reqHeaderString);
                writer.flush();

                //receive response
                reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                stringBuilder = new StringBuilder();


                //read header
                for (String line; (line = reader.readLine()) != null; ) {
                    stringBuilder.append(line).append("\n");
                    System.out.println("received header: "+line);

                    //stop reading if header end is reached
                    if(line.isEmpty()){
                        retResponse.setHeader(stringBuilder.toString());
                        stringBuilder = new StringBuilder();
                        break;
                    }
                }

                //read body
                if(!retResponse.getHeader().contains("Content-Length: 0")){
                    String line;
                    while(reader.ready() && (line = reader.readLine()) != null && !line.contains("%%EOF") && !line.contains("</html>") ){
                        stringBuilder.append(line).append("\n");
                        System.out.println("received body: "+line);
                    }

                    retResponse.setBody(stringBuilder.toString());
                }else {
                    retState = RequestState.HEADER_ONLY;
                }
            }catch (UnknownHostException e){
                retState = RequestState.UNKNOWN_HOST;
                e.printStackTrace();
            }catch (Exception e){
                retState = RequestState.ERROR;
                retResponse.setBody(e.getMessage());
                e.printStackTrace();
            }finally {
                //clear up
                if (reader != null){
                    try {
                        reader.close();
                    } catch (IOException ignore) {}
                }
                if (writer != null) {
                    writer.close();
                }
                if (socket != null){
                    try {
                        socket.close();
                    } catch (IOException ignore) {}
                }
            }
        }else {
            //header build failed so return error
            retState = RequestState.ERROR;
        }
        return new Pair<>(retState, retResponse);
    }

    public RequestHeader getReqHeader() {
        return reqHeader;
    }

    public Response getResponse() {
        return response;
    }

    public RequestState getState() {
        return state;
    }
}
