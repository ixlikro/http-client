package model;

import main_window.Controller;

import java.util.*;

public class RequestHeader {

    private String domain;
    private String path;
    private int port;
    private Map<String, String> allHeaderArguments;
    private Map<String, String> allPostArguments;
    private HttpRequestType reqType;


    public RequestHeader() {
        allHeaderArguments = new HashMap<>();
        allPostArguments = new HashMap<>();

        domain = null;
        path = null;
        reqType = null;
        port = Controller.INVALID;
    }

    public synchronized String buildHeader(){

        //all data available?
        if (domain == null || path == null || reqType == null || port == Controller.INVALID) {
            return null;
        }

        //build now the post data so we know how long it is. The length is needed for the Content-Length header argument
        StringBuilder postContent = null;
        if(reqType == HttpRequestType.POST){
            postContent = new StringBuilder();
            for (Map.Entry<String, String> arg : allPostArguments.entrySet()) {
                postContent.append(arg.getKey())
                        .append('=')
                        .append(arg.getValue())
                        .append("&");
            }
            //replace the last '&' with the line end
            if(postContent.length() != 0){
                postContent.replace(postContent.length()-1, postContent.length(),"\r\n");
            }
        }

        //add some default header arguments if they are not set jet
        if(!allHeaderArguments.containsKey("User-Agent")){
            allHeaderArguments.put("User-Agent", "JavaFx HTTP Client");
        }
        if(!allHeaderArguments.containsKey("Accept")){
            allHeaderArguments.put("Accept", "*/*");
        }

        //add the post specific header arguments
        if(reqType == HttpRequestType.POST){
            if(!allHeaderArguments.containsKey("Content-Length")){
                allHeaderArguments.put("Content-Length", postContent.length()+"");
            }
            if(!allHeaderArguments.containsKey("Content-Type")){
                allHeaderArguments.put("Content-Type", "multipart/form-data");
            }
        }

        StringBuilder builder = new StringBuilder();

        //add minimal header
        builder.append(reqType.name())
                .append(' ')
                .append(path)
                .append(' ')
                .append("HTTP/1.1\r\n")
                .append("Host: ")
                .append(getHostName())
                .append("\r\n");

        //add request args
        for(Map.Entry<String,String> arg : allHeaderArguments.entrySet()){
            builder.append(arg.getKey())
                    .append(": ")
                    .append(arg.getValue())
                    .append("\r\n");
        }

        //add empty row to identify the end of the header
        builder.append("\r\n");

        if(reqType == HttpRequestType.POST){
            builder.append(postContent.toString());
        }

        return builder.toString();
    }

    /**
     * @return the host name, in other words this method cut of the heading http:// from the domain and return it
     */
    public String getHostName(){
        if(domain != null){
            //7, because http:// is 7 chars long
            return domain.substring(7);
        }else {
            return null;
        }
    }

    public void addHeaderArgument(String key, String value){
        allHeaderArguments.put(key, value);
    }

    public void removeAllHeaderArguments(){
        allHeaderArguments.clear();
    }

    public void addPostArgument(String key, String value){
        allPostArguments.put(key, value);
    }

    public void removeAllPostArguments(){
        allPostArguments.clear();
    }

    public String getDomain() {
        return domain;
    }

    public synchronized void setDomain(String domain) {
        this.domain = domain;
    }

    public String getPath() {
        return path;
    }

    public synchronized void setPath(String path) {
        this.path = path;
    }

    public HttpRequestType getReqType() {
        return reqType;
    }

    public void setReqType(HttpRequestType reqType) {
        this.reqType = reqType;
    }

    public int getPort() {
        return port;
    }

    public synchronized void setPort(int port) {
        this.port = port;
    }
}
