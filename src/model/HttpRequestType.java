package model;

public enum HttpRequestType {
    GET, POST;
}
