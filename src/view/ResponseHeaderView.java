package view;

import javafx.scene.control.Label;
import main_window.Controller;


public class ResponseHeaderView extends View {

    private Label label;

    public ResponseHeaderView(Controller controller) {
        super(controller);
    }

    @Override
    public void updateUI() {
        switch (controller.getRequest().getState()) {
            case NOT_SEND_YET:
                label.setText(Message.NOT_SEND_YET);
                break;
            case PENDING:
                label.setText(Message.PENDING);
                break;
            case TIMEOUT:
                label.setText(Message.TIMEOUT);
                break;
            case ERROR:
                label.setText(Message.ERROR);
                break;
            case UNKNOWN_HOST:
                label.setText(Message.UNKNOWN_HOST);
                break;
            case HEADER_ONLY: case RESPONSE_RECEIVED:
                label.setText(controller.getRequest().getResponse().getHeader());
                break;
        }
    }

    @Override
    public void setupUI() {
        label = controller.outputResponseHeader;
        label.setText(Message.NOT_SEND_YET);
    }

    @Override
    public void resetUI() {
        label.setText(Message.NOT_SEND_YET);
    }
}
