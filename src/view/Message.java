package view;

import javafx.scene.control.Tooltip;
import javafx.scene.text.Font;
import javafx.util.Duration;

import java.util.function.Function;

/**
 * This class provides the longer messages that are displayed in the main window
 */
public class Message {

    static public String NOT_SEND_YET = "Noch kein Request abgeschickt.";
    static public String TIMEOUT = "Es ist ein Timeout aufgetreten!";
    static public String UNKNOWN_HOST = "Unknown Host!\nHast du dich eventuell bei der Webadresse vertippt?";
    static public String ERROR = "Es ist ein Fehler aufgetreten!";
    static public String ERROR_TRAIL = " Vielleicht hilft das hier ja: \n";
    static public String PENDING = "Laden...";
    static public String HEADER_ONLY = "Es wurde kein Body mitgesendet.";

    static public String OUTPUT_LINK_COUNT = " externe Links gefunden.";
    static public String OUTPUT_BODY_TO_LARGE_1 = "Es hat alles geklappt!\nAber der empfangende Body enthält ";
    static public String OUTPUT_BODY_TO_LARGE_2 = " Zeichen, etwas zu viel für ein Label...\n\nWenn man den Body als Datei speichert kann man ihn trotzdem über einen Editor anschauen.";

    static public Tooltip ADDRESS_TOOLTIP, Arguments_TOOLTIP;
    static {

        Function<String, Tooltip> createTooltip = s -> {
            Tooltip tip = new Tooltip(s);
            tip.setShowDelay(Duration.millis(50));
            tip.setFont(new Font(tip.getFont().getFamily(),12));
            tip.setShowDuration(Duration.INDEFINITE);
            return tip;
        };

        ADDRESS_TOOLTIP = createTooltip.apply("Mögliche gültige Eingaben:\n- domain.de\n- sub.domain.de\n- www.domain.de\n- http://www.domain.de");
        Arguments_TOOLTIP = createTooltip.apply("Argumente werden durch ein ';' getrennt!\nSyntax Header-Argument-> 'key : value'\nSyntax POST-Argument-> 'key = value'\n\nBeispiel:\nsecKey: geheimer Key; firstName = Ole;\nlastName=Schleesselmann;");
    }
}
