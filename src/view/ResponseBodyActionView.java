package view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Tooltip;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import main_window.Controller;
import utility.ExportResponseBody;
import utility.LastStorePath;
import webView_window.ControllerWebView;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

public class ResponseBodyActionView extends View {

    private Tooltip tooltip;

    public ResponseBodyActionView(Controller controller) {
        super(controller);
    }

    @Override
    public void updateUI() {
        switch (controller.getRequest().getState()) {
            case RESPONSE_RECEIVED:
                performOnAllNodes(node -> node.setVisible(true), false);

                //find links in body
                List<String> links = controller.getRequest().getResponse().findLinksInBody();

                if(links.size() != 0){
                    controller.outputLinkCount.setText(links.size()+ Message.OUTPUT_LINK_COUNT);

                    //build string for the tooltip
                    StringBuilder stringBuilder = new StringBuilder();
                    links.forEach(s -> {
                        stringBuilder.append(s);
                        stringBuilder.append("\n");
                    });
                    tooltip.setText(stringBuilder.toString());

                    controller.outputLinkCount.setVisible(true);
                }else {
                    controller.outputLinkCount.setVisible(false);
                }

                break;
            default:
                performOnAllNodes(node -> node.setVisible(false),true);
                break;
        }
    }

    @Override
    public void setupUI() {
        performOnAllNodes(node -> node.setVisible(false),true);

        //setup tooltip for the link count output
        tooltip = new Tooltip("");
        tooltip.setShowDelay(Duration.millis(50));
        tooltip.setFont(new Font(tooltip.getFont().getFamily(),12));
        tooltip.setShowDuration(Duration.INDEFINITE);
        Tooltip.install(controller.outputLinkCount, tooltip);

        //save body in file listener
        controller.saveInFileBtn.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Response body als Datei speichern");
            fileChooser.setInitialFileName(controller.getRequest().getReqHeader().getHostName());
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("HTML-Datei","*.html"));

            String lastPath = LastStorePath.getInstance().getPath();
            if(lastPath != null){
                fileChooser.setInitialDirectory(new File(lastPath));
            }

            File file = fileChooser.showSaveDialog(controller.getStage());
            if (file != null) {
                LastStorePath.getInstance().setPath(file);

                ExportResponseBody.getInstance().export(controller.getRequest().getResponse().getBody(), file);
            }
        });

        //show as web view button listener
        controller.showInWebViewBtn.setOnAction(event -> {

            //store body in file
            File file = new File(Controller.WEB_VIEW_FILE_PATH);
            ExportResponseBody.getInstance().export(controller.getRequest().getResponse().getBody(),file);

            //open web view in a separate window
            ControllerWebView webViewController = null;
            Parent root = null;
            Stage stage = null;
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../webView_window/webViewWindow.fxml"));
            try {
                root = fxmlLoader.load();
                stage = new Stage();
                stage.setScene(new Scene(root, 1000, 800));
                webViewController = fxmlLoader.getController();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(webViewController != null){
            stage.setMinWidth(200);
            stage.setMinHeight(200);
            stage.initOwner(controller.getStage());
            stage.setTitle("WebView - " + controller.getRequest().getReqHeader().getDomain()
                    + controller.getRequest().getReqHeader().getPath());

            stage.show();
            webViewController.setup(stage, file.getPath());
            } else {
                System.err.println("Loading web view gone horribly wrong.");
            }
        });
    }

    @Override
    public void resetUI() {
        performOnAllNodes(node -> node.setVisible(false),true);
    }

    private void performOnAllNodes(Consumer<Node> consumer, boolean withLinkCountOutput){
        if(withLinkCountOutput)
            consumer.accept(controller.outputLinkCount);
        consumer.accept(controller.saveInFileBtn);
        consumer.accept(controller.showInWebViewBtn);
    }


}
