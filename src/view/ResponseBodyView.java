package view;

import javafx.scene.control.Label;
import main_window.Controller;

public class ResponseBodyView extends View {
    private Label label;

    public ResponseBodyView(Controller controller) {
        super(controller);
    }

    @Override
    public void updateUI() {
        switch (controller.getRequest().getState()) {
            case NOT_SEND_YET:
                label.setText(Message.NOT_SEND_YET);
                break;
            case PENDING:
                label.setText(Message.PENDING);
                break;
            case TIMEOUT:
                label.setText(Message.TIMEOUT);
                break;
            case ERROR:
                label.setText(Message.ERROR);
                if(controller.getRequest().getResponse().getBody() != null){
                    label.setText(label.getText()+ Message.ERROR_TRAIL + controller.getRequest().getResponse().getBody());
                }
                break;
            case UNKNOWN_HOST:
                label.setText(Message.UNKNOWN_HOST);
                break;
            case HEADER_ONLY:
                label.setText(Message.HEADER_ONLY);
                break;
            case RESPONSE_RECEIVED:
                //ok, very long bodies crashing the main thread with the label.setText call...
                if(controller.getRequest().getResponse().getBody().length() > 500000){
                    label.setText(Message.OUTPUT_BODY_TO_LARGE_1 + controller.getRequest().getResponse().getBody().length() + Message.OUTPUT_BODY_TO_LARGE_2);
                }else {
                    label.setText(controller.getRequest().getResponse().getBody());
                }
                break;
        }
    }

    @Override
    public void setupUI() {
        label = controller.outputResponseBody;
        label.setText(Message.NOT_SEND_YET);
    }

    @Override
    public void resetUI() {
        label.setText(Message.NOT_SEND_YET);
    }
}
