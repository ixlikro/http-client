package view;

import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.paint.Color;

import java.util.function.Function;

/**
 * This class provide 3 shadow/glow effects that can set on every
 * {@link javafx.scene.Node}-Object using the method {@link javafx.scene.Node#setEffect(Effect)}.
 */
public class ShadowEffect {
    static public Effect RED, GREEN, BLUE;

    static{
        Function<Color, DropShadow> createShadow = (color) -> {
            DropShadow temp= new DropShadow();
            temp.setOffsetY(0f);
            temp.setOffsetX(0f);
            temp.setColor(color);
            temp.setWidth(15);
            temp.setHeight(15);
            return temp;
        };
        BLUE = createShadow.apply(Color.BLUE);
        RED = createShadow.apply(Color.RED);
        GREEN = createShadow.apply(Color.GREEN);
    }
}
