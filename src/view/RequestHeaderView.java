package view;

import main_window.Controller;

public class RequestHeaderView extends View {

    public RequestHeaderView(Controller controller) {
        super(controller);
    }

    @Override
    public void updateUI() {
        switch (controller.getRequest().getState()) {
            case NOT_SEND_YET:
                controller.outputRequestHeader.setText(Message.NOT_SEND_YET);
                break;
            default:
                controller.outputRequestHeader.setText(controller.getRequest().getReqHeader().buildHeader());
                break;
        }
    }

    @Override
    public void setupUI() {
        controller.outputRequestHeader.setText(Message.NOT_SEND_YET);
    }

    @Override
    public void resetUI() {
        controller.outputRequestHeader.setText(Message.NOT_SEND_YET);
    }
}
