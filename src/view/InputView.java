package view;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import main_window.Controller;
import model.HttpRequestType;
import model.RequestState;
import utility.LocalMethod;


public class InputView extends View {

    //we need to store a reference to the changeListener otherwise would the gc collect the listener (WeakChangeListener).
    //we use the weak listener to prevent memory leaks. So now its save to call multiple times the setupUI-method.
    private ChangeListener<Boolean> webAddressFocusListener, portFocusListener, argsFocusListener ;


    public InputView(Controller controller) {
        super(controller);
    }

    @Override
    public void updateUI() {
        switch (controller.getRequest().getState()) {
            case PENDING:
                controller.inputSendRequest.setDisable(true);
                controller.inputReset.setDisable(true);
                break;
            default:
                controller.inputSendRequest.setDisable(false);
                controller.inputReset.setDisable(false);
                break;
        }
    }

    @Override
    public void setupUI() {
        //set initial values (METHOD = GET, Port = 80)
        controller.inputRadioGroup.selectToggle(controller.inputRadioButtonGet);
        controller.inputPort.setText(Controller.DEFAULT_PORT+"");

        //add question mark the web address
        controller.inputWebAddressHelp.setImage(new Image("questionMark.png", 15,15
                , true, true));
        Tooltip.install(controller.inputWebAddressHelp, Message.ADDRESS_TOOLTIP);
        //add question mark the argument
        controller.inputRequestArgumentsHelp.setImage(new Image("questionMark.png", 15,15
                , true, true));
        Tooltip.install(controller.inputRequestArgumentsHelp, Message.Arguments_TOOLTIP);

        controller.inputWebAddress.setOnAction(event -> {
            controller.inputSendRequest.requestFocus();
            if(controller.getRequest().getState() != RequestState.PENDING){
                sendRequest();
            }
        });

        //initialize the focus listeners
        webAddressFocusListener = (observable, oldValue, newValue) -> {
            if(newValue){
                //got focus
                controller.inputWebAddress.setEffect(null);
                selectAll(controller.inputWebAddress);
            }else {
                //lost focus
                checkAndGetWebAddressFromInput();
            }
        };
        portFocusListener = (observable, oldValue, newValue) -> {
            if(newValue){
                selectAll(controller.inputPort);
            }else {
                checkAndGetPortFromInput();
            }
        };
        argsFocusListener = (observable, oldValue, newValue) -> {
            if(!newValue){
                if(!checkAndGetArgsFromInput()){
                    controller.inputRequestArguments.setEffect(ShadowEffect.RED);
                }
            }else {
                controller.inputRequestArguments.setEffect(null);
            }
        };

        //force port input field to be numeric only
        controller.inputPort.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                controller.inputPort.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });

        //set onAction listener on the send request button
        controller.inputSendRequest.setOnAction(event -> {
            sendRequest();
        });

        controller.inputReset.setOnAction(event -> {
            controller.reset();
        });

        //set the focus listener as weak listener
        controller.inputWebAddress.focusedProperty().addListener(new WeakChangeListener<>(webAddressFocusListener));
        controller.inputPort.focusedProperty().addListener(new WeakChangeListener<>(portFocusListener));
        controller.inputRequestArguments.focusedProperty().addListener(new WeakChangeListener<>(argsFocusListener));
    }

    @Override
    public void resetUI() {
        controller.inputWebAddress.setText("");
        controller.inputPort.setText(Controller.DEFAULT_PORT+"");
        controller.inputRequestArguments.setText("");
    }

    /**
     * provides a basic domain validation.
     * this method will add 'http://' OR 'www.' OR '/' to the address if the user don't type it.
     * if the input is valid, this method set the domain and path in the {@link model.Request} from the controller automatically
     * if the input is invalid, this method set the domain and path to NULL in the {@link model.Request} from the controller.
     *
     * @return true: a 'valid' address, false: a invalid address
     */
    private boolean checkAndGetWebAddressFromInput(){
        String input = controller.inputWebAddress.getText();
        LocalMethod invalid = () -> {
            //reset the domain and the path inside the Request item from the controller
            controller.getRequest().getReqHeader().setDomain(null);
            controller.getRequest().getReqHeader().setPath(null);

            controller.inputWebAddress.setEffect(ShadowEffect.RED);
        };

        if(input.contains(",") || input.contains(" ") || !input.contains(".")){
            invalid.run();
            return false;
        }

        //check if we have to add some basic domain parts
        if(!input.startsWith("http://")){
            input = "http://" + input;
        }

        if(!input.startsWith("http://www.")){
            input = "http://www." + input.substring(7);
        }

        if(!input.substring(11).contains("/")){
            input = input + '/';
        }


        if(input.matches("http://www.(\\w+.)+(\\w+)/?")){
            //valid input

            //split the input to domain and path
            int pathStart = findDomainPathDivider(input);
            if(pathStart != Controller.INVALID){

                //set the domain and the path inside the Request item from the controller
                controller.getRequest().getReqHeader().setPath(input.substring(pathStart));
                controller.getRequest().getReqHeader().setDomain(input.substring(0, pathStart));
            }

            //set the input string and remove the shadow effect from the textField
            controller.inputWebAddress.setText(input);
            controller.inputWebAddress.setEffect(null);
        }else {
            //invalid input
            invalid.run();
            return false;
        }

        return true;
    }

    private void sendRequest(){
        if(controller.inputRadioGroup.getSelectedToggle() == controller.inputRadioButtonGet){
            controller.getRequest().getReqHeader().setReqType(HttpRequestType.GET);
        }else {
            controller.getRequest().getReqHeader().setReqType(HttpRequestType.POST);
        }

        if (checkAndGetArgsFromInput() && checkAndGetWebAddressFromInput() && checkAndGetPortFromInput()) {
            controller.sendRequest();
        }
    }

    /**
     * check if the user entered a valid port
     * if the input is valid, this method set the port in the {@link model.Request} from the controller automatically
     * if the input is invalid, this method set the port in the {@link model.Request} and the textField to {@link Controller#DEFAULT_PORT}
     * @return true: a valid port, false: a invalid port
     */
    private boolean checkAndGetPortFromInput(){

        LocalMethod invalid = () -> {
            controller.getRequest().getReqHeader().setPort(Controller.DEFAULT_PORT);
            controller.inputPort.setText(Controller.DEFAULT_PORT+"");
        };

        if(controller.inputPort.getText().isEmpty()){
            invalid.run();
            return false;
        }

        int port;
        try {
            port = Integer.parseInt(controller.inputPort.getText());
        } catch (NumberFormatException e) {
            invalid.run();
            return false;
        }

        if(port >= 0 && port <= 65535){
            controller.getRequest().getReqHeader().setPort(port);
            return true;
        }

        invalid.run();
        return false;
    }

    /**
     * check if the user entered valid header and post arguments.
     * if the input is valid, this method set the header arguments in the {@link model.Request} from the controller automatically
     * if the input is invalid, this method delete all header arguments in the {@link model.Request} from the controller.
     * @return true: valid arguments, false: invalid port
     */
    private boolean checkAndGetArgsFromInput(){
        //make sure that we don't keep old data
        controller.getRequest().getReqHeader().removeAllHeaderArguments();
        controller.getRequest().getReqHeader().removeAllPostArguments();

        //if the text area is empty just return true
        if(controller.inputRequestArguments.getText().isEmpty()){
            controller.inputRequestArguments.setEffect(null);
            return true;
        }


        String input = controller.inputRequestArguments.getText();
        String[] args = input.split(";");
        boolean valid = true;

        for(String arg : args){
            String[] nameAndArgHeader = arg.trim().split(":");
            String[] nameAndArgPost = arg.trim().split("=");

            if(nameAndArgHeader.length != 2 && nameAndArgPost.length != 2){
                valid = false;
                break;
            }else {
                if(nameAndArgHeader.length == 2 ){
                    if(nameAndArgHeader[0].trim().contains(" ")){
                        valid = false;
                        break;
                    }

                    //add the valid header argument
                    controller.getRequest().getReqHeader().addHeaderArgument(nameAndArgHeader[0].trim(), nameAndArgHeader[1].trim());
                }
                if(nameAndArgPost.length == 2){
                    if(nameAndArgPost[0].trim().contains(" ")){
                        valid = false;
                        break;
                    }

                    //add the valid post request argument
                    controller.getRequest().getReqHeader().addPostArgument(nameAndArgPost[0].trim(), nameAndArgPost[1].trim());
                }
            }
        }

        if(!valid){
            //reset data and set red shadow effect
            controller.getRequest().getReqHeader().removeAllHeaderArguments();
            controller.getRequest().getReqHeader().removeAllPostArguments();
            controller.inputRequestArguments.setEffect(ShadowEffect.RED);
            return false;
        }else {
            controller.inputRequestArguments.setEffect(null);
        }

        return true;
    }

    /**
     * find the divider that dissociates the domain and the path (the 3rd slash)
     * @param url a well formatted url with a http:// at the start
     * @return the index of the 3rd slash or Controller.INVALID if it not exists
     */
    private int findDomainPathDivider(String url){
        int count = 0;

        for (int i = 0; i < url.length(); i++){
            if(url.charAt(i) == '/'){
                count++;
            }

            if(count == 3){
                return i;
            }
        }

        return Controller.INVALID;
    }

    /**
     * Select every character in the given {@link TextField}.
     *
     * For some weird reason will the {@link TextField#selectAll()}-Method not work if it called from a listener.
     * However this will work bc it use the {@link Platform#runLater(Runnable)} workaround.
     *
     * @param textField the {@link TextField} that text should be selected
     */
    private void selectAll(TextField textField){
        Platform.runLater(() -> {
            if (textField.isFocused() && !textField.getText().isEmpty()) {
                textField.selectAll();
            }
        });
    }

}
