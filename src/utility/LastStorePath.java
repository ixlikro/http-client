/*
 * CpC - Carport calculator
 *
 * Copyright (c) 2018. by Ole Schleeßelmann.
 */

package utility;

import java.io.*;

/**
 * This singleton class save the last path that was chosen from the user to save the response body
 */
public class LastStorePath {
    private static LastStorePath ourInstance;
    private static String filePath = "lastStorePath.txt";

    public static LastStorePath getInstance() {
        if(ourInstance == null){
            ourInstance = new LastStorePath();
        }
        return ourInstance;
    }

    private LastStorePath() {

    }

    public void setPath(File file){
        try(BufferedWriter writer =  new BufferedWriter(new FileWriter(filePath))) {
            writer.write(file.getParent());
        } catch (Exception e) {
            System.err.println("Error while writing the lastStorePath.txt");
            e.printStackTrace();
        }
    }

    public String getPath(){
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))){
            return reader.readLine();
        } catch (FileNotFoundException e) {
            //can happen for example when the user press the save button the first time
            return null;
        } catch (IOException e) {
            System.err.println("Error while reading the lastStorePath.txt");
            e.printStackTrace();
        }
        return null;
    }
}
