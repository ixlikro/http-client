package utility;


/**
 * Just a small functional interface to use as local method (inside another method)
 *
 * There is no build in functional interface like {@link java.util.function.Function} or {@link java.util.function.Consumer}
 * for a method that takes nothing and returns nothing.
 */
@FunctionalInterface
public interface LocalMethod {
    void run();
}
