/*
 * CpC - Carport calculator
 *
 * Copyright (c) 2018. by Ole Schleeßelmann.
 */

package utility;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;


public class ExportResponseBody {
    private static ExportResponseBody ourInstance;

    public static ExportResponseBody getInstance() {
        if(ourInstance == null){
            ourInstance = new ExportResponseBody();
        }
        return ourInstance;
    }

    public void export(String body, File file){
        String pathAndName = file.toString();
        if (!pathAndName.endsWith(".html")){
            pathAndName += ".html";
        }
        try(BufferedWriter writer =  new BufferedWriter(new FileWriter(pathAndName))) {
            writer.write(body);
        } catch (Exception e) {
            System.err.println("Error while exporting "+file.getName()+" File!");
            e.printStackTrace();
        }
    }
}
