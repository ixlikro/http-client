import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main_window.Controller;

import java.io.IOException;

public class Main extends Application {

    private Controller controller;

    @Override
    public void start(Stage primaryStage){
        Parent root = null;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("main_window/mainWindow.fxml"));
        try {
            root = fxmlLoader.load();
            primaryStage.setScene(new Scene(root, 1200, 500));
            controller = fxmlLoader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
        primaryStage.setMinWidth(600);
        primaryStage.setMinHeight(200);
        primaryStage.setTitle("HTTP Client");

        if (controller != null) {
            controller.setup(primaryStage);
            primaryStage.show();
        } else {
            System.err.println("Loading main window gone horribly wrong -_-\nPlease restart the Application");
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
