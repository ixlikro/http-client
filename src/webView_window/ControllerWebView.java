package webView_window;

import javafx.fxml.FXML;
import javafx.scene.web.WebView;
import javafx.stage.Stage;


import java.io.File;
import java.net.MalformedURLException;


public class ControllerWebView {
    @FXML public WebView outputWebView;

    private Stage stage;

    public ControllerWebView() {
    }

    public void setup(Stage stage, String fileName) {
        this.stage = stage;

        try {
            outputWebView.getEngine().load(new File(System.getProperty("user.dir") + File.separator + fileName)
                    .toURI().toURL().toExternalForm());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }


    public Stage getStage() {
        return stage;
    }

}
